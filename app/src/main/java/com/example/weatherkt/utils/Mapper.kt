package com.example.weatherkt.utils

interface Mapper<R, D> {
    fun mapFrom(type: R): D
}
