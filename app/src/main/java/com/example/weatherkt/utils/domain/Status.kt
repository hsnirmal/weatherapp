package com.example.weatherkt.utils.domain

// references :
// https://developer.android.com/jetpack/docs/guide#addendum

enum class Status {
    SUCCESS,
    LOADING,
    ERROR
}
