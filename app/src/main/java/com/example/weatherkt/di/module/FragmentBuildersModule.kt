package com.example.weatherkt.di.module

import com.example.weatherkt.ui.dashboard.DashboardFragment
import com.example.weatherkt.ui.search.SearchFragment
import com.example.weatherkt.ui.splash.SplashFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeSplashFragment(): SplashFragment

    @ContributesAndroidInjector
    abstract fun contributeDashboardFragment(): DashboardFragment

    @ContributesAndroidInjector
    abstract fun contributeSearchFragment(): SearchFragment
}
