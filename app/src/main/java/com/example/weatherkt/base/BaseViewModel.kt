package com.example.weatherkt.base

import androidx.lifecycle.ViewModel


open class BaseViewModel : ViewModel()
