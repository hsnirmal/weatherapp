package com.example.weatherkt.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.weatherkt.db.dao.CitiesForSearchDao
import com.example.weatherkt.db.dao.CurrentWeatherDao
import com.example.weatherkt.db.dao.ForecastDao
import com.example.weatherkt.db.entity.CitiesForSearchEntity
import com.example.weatherkt.db.entity.CurrentWeatherEntity
import com.example.weatherkt.db.entity.ForecastEntity
import com.example.weatherkt.utils.typeconverters.DataConverter

@Database(
    entities = [
        ForecastEntity::class,
        CurrentWeatherEntity::class,
        CitiesForSearchEntity::class
    ],
    version = 2
)
@TypeConverters(DataConverter::class)
abstract class WeatherDatabase : RoomDatabase() {

    abstract fun forecastDao(): ForecastDao

    abstract fun currentWeatherDao(): CurrentWeatherDao

    abstract fun citiesForSearchDao(): CitiesForSearchDao
}
