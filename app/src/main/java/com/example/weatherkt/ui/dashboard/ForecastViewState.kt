package com.example.weatherkt.ui.dashboard

import com.example.weatherkt.base.BaseViewState
import com.example.weatherkt.db.entity.ForecastEntity
import com.example.weatherkt.utils.domain.Status


class ForecastViewState(
    val status: Status,
    val error: String? = null,
    val data: ForecastEntity? = null
) : BaseViewState(status, error) {
    fun getForecast() = data
}
