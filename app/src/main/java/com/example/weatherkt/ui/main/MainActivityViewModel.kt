package com.example.weatherkt.ui.main

import androidx.databinding.ObservableField
import com.example.weatherkt.base.BaseViewModel
import javax.inject.Inject

class MainActivityViewModel @Inject internal constructor() : BaseViewModel() {
    var toolbarTitle: ObservableField<String> = ObservableField()
}
