package com.example.weatherkt.ui.splash

import android.graphics.Color
import androidx.navigation.fragment.findNavController
import com.example.weatherkt.base.BaseFragment
import com.example.weatherkt.base.Constants
import com.example.weatherkt.databinding.FragmentSplashBinding
import com.example.weatherkt.utils.extensions.hide
import com.example.weatherkt.utils.extensions.show
import com.mikhaellopez.rxanimation.*
import io.reactivex.disposables.CompositeDisposable
import com.example.weatherkt.R

class SplashFragment : BaseFragment<SplashFragmentViewModel, FragmentSplashBinding>(
    R.layout.fragment_splash,
    SplashFragmentViewModel::class.java
) {

    var disposable = CompositeDisposable()

    override fun init() {
        super.init()

        binding.viewModel?.navigateDashboard = true

        binding.viewModel?.navigateDashboard?.let { startSplashAnimation(it) }

    }

    private fun startSplashAnimation(navigateToDashboard: Boolean) {
        disposable.add(
            RxAnimation.sequentially(
                RxAnimation.together(
                    binding.txtSplashName.translationY(-500f, 3000),
                    binding.txtSplashName.scale(2.0f, 3000),
                    binding.txtSplashName.alpha(0.1f, 3000)
                )
            ).doOnTerminate {
                findNavController().graph.startDestination =
                    R.id.dashboardFragment // Little bit tricky solution :)
                if (navigateToDashboard)
                    navigate(R.id.action_splashFragment_to_dashboardFragment)
            }
                .subscribe()
        )
    }

   /* private fun endSplashAnimation(navigateToDashboard: Boolean) {
        disposable.add(
            RxAnimation.sequentially(
                binding.rootView.backgroundColor(
                    Color.parseColor("#8885A6"),
                    Color.parseColor("#89879F"),
                    duration = 750L
                )
            )
                .doOnTerminate {
                    findNavController().graph.startDestination =
                        R.id.dashboardFragment // Little bit tricky solution :)
                    if (navigateToDashboard)
                        navigate(R.id.action_splashFragment_to_dashboardFragment)
                    else
                        navigate(R.id.action_splashFragment_to_searchFragment)
                }
                .subscribe()

        )
    }*/

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }
}
