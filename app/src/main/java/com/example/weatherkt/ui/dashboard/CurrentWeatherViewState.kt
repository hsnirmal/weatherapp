package com.example.weatherkt.ui.dashboard

import com.example.weatherkt.base.BaseViewState
import com.example.weatherkt.db.entity.CurrentWeatherEntity
import com.example.weatherkt.utils.domain.Status

class CurrentWeatherViewState(
    val status: Status,
    val error: String? = null,
    val data: CurrentWeatherEntity? = null
) : BaseViewState(status, error) {
    fun getForecast() = data
}
