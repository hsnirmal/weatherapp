package com.example.weatherkt.ui.search

import com.example.weatherkt.base.BaseViewState
import com.example.weatherkt.db.entity.CitiesForSearchEntity
import com.example.weatherkt.utils.domain.Status

class SearchViewState(
    val status: Status,
    val error: String? = null,
    val data: List<CitiesForSearchEntity>? = null
) : BaseViewState(status, error) {
    fun getSearchResult() = data
}
