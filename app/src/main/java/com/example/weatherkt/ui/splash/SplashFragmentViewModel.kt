package com.example.weatherkt.ui.splash

import android.content.SharedPreferences
import com.example.weatherkt.base.BaseViewModel
import javax.inject.Inject

class SplashFragmentViewModel @Inject constructor(var sharedPreferences: SharedPreferences) : BaseViewModel() {
    var navigateDashboard = false
}
