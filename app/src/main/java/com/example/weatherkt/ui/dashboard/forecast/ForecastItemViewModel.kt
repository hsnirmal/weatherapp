package com.example.weatherkt.ui.dashboard.forecast

import androidx.databinding.ObservableField
import com.example.weatherkt.base.BaseViewModel
import com.example.weatherkt.domain.model.ListItem
import javax.inject.Inject

class ForecastItemViewModel @Inject internal constructor() : BaseViewModel() {
    var item = ObservableField<ListItem>()
}
