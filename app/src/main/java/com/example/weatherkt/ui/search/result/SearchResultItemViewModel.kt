package com.example.weatherkt.ui.search.result

import androidx.databinding.ObservableField
import com.example.weatherkt.base.BaseViewModel
import com.example.weatherkt.db.entity.CitiesForSearchEntity
import javax.inject.Inject

class SearchResultItemViewModel @Inject internal constructor() : BaseViewModel() {
    var item = ObservableField<CitiesForSearchEntity>()
}
