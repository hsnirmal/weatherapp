package com.example.weatherkt.ui.search

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import com.example.weatherkt.base.BaseViewModel
import com.example.weatherkt.base.Constants
import com.example.weatherkt.db.entity.CitiesForSearchEntity
import com.example.weatherkt.db.entity.CoordEntity
import com.example.weatherkt.domain.usecase.SearchCitiesUseCase
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SearchViewModel @Inject internal constructor(private val useCase: SearchCitiesUseCase, private val pref: SharedPreferences) : BaseViewModel() {

    private val _searchParams: MutableLiveData<SearchCitiesUseCase.SearchCitiesParams> = MutableLiveData()
    fun getSearchViewState() = searchViewState

    private val searchViewState: LiveData<SearchViewState> = _searchParams.switchMap { params ->
        useCase.execute(params)
    }

    fun setSearchParams(params: SearchCitiesUseCase.SearchCitiesParams) {
        if (_searchParams.value == params)
            return
        _searchParams.postValue(params)
    }

    fun saveCoordsToShaerdPref(citiesForSearchEntity: CitiesForSearchEntity): Single<String>? {
        return Single.create<String> {
            val name = citiesForSearchEntity.name+", "+citiesForSearchEntity.administrative+" "+citiesForSearchEntity.country
            pref.edit().putString(Constants.Coords.LAT, citiesForSearchEntity.coord!!.lat.toString()).apply()
            pref.edit().putString(Constants.Coords.LON, citiesForSearchEntity.coord!!.lon.toString()).apply()
            pref.edit().putString(Constants.Place.NAME, name).apply()
            it.onSuccess("")
        }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }
}
