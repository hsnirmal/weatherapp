package com.example.weatherkt.ui.dashboard

import android.transition.TransitionInflater
import android.view.View
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weatherkt.R
import com.example.weatherkt.base.BaseFragment
import com.example.weatherkt.base.Constants
import com.example.weatherkt.databinding.FragmentDashboardBinding
import com.example.weatherkt.domain.model.ListItem
import com.example.weatherkt.domain.usecase.CurrentWeatherUseCase
import com.example.weatherkt.domain.usecase.ForecastUseCase
import com.example.weatherkt.ui.dashboard.forecast.ForecastAdapter
import com.example.weatherkt.ui.main.MainActivity
import com.example.weatherkt.utils.extensions.isNetworkAvailable
import com.example.weatherkt.utils.extensions.observeWith

class DashboardFragment : BaseFragment<DashboardFragmentViewModel, FragmentDashboardBinding>(R.layout.fragment_dashboard, DashboardFragmentViewModel::class.java){

    override fun init() {
        super.init()
        initForecastAdapter()
        sharedElementReturnTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)

        val lat: String? = binding.viewModel?.sharedPreferences?.getString(Constants.Coords.LAT, "")
        val lon: String? = binding.viewModel?.sharedPreferences?.getString(Constants.Coords.LON, "")
        val place: String? = binding.viewModel?.sharedPreferences?.getString(Constants.Place.NAME, "Unknown")

        if(place?.isNotEmpty() == true){
            with(binding) {
                containerForecast.placeText = place
            }
        }

        if (lat?.isNotEmpty() == true && lon?.isNotEmpty() == true) {
            binding.viewModel?.setCurrentWeatherParams(CurrentWeatherUseCase.CurrentWeatherParams(lat, lon, isNetworkAvailable(requireContext()), Constants.Coords.METRIC))
            binding.viewModel?.setForecastParams(ForecastUseCase.ForecastParams(lat, lon, isNetworkAvailable(requireContext()), Constants.Coords.METRIC))
            binding.scrollView.visibility = View.VISIBLE
            binding.txtNoLocation.visibility = View.GONE
        }else{
            binding.scrollView.visibility = View.GONE
            binding.txtNoLocation.visibility = View.VISIBLE
        }

        binding.viewModel?.getForecastViewState()?.observeWith(
            viewLifecycleOwner
        ) {
            with(binding) {
                viewState = it
                it.data?.list?.let { forecasts -> initForecast(forecasts) }
                (activity as MainActivity).viewModel.toolbarTitle.set(it.data?.city?.getCityAndCountry())
            }
        }

        binding.viewModel?.getCurrentWeatherViewState()?.observeWith(
            viewLifecycleOwner
        ) {
            with(binding) {
                containerForecast.viewState = it
            }
        }
    }

    private fun initForecastAdapter() {
        val adapter = ForecastAdapter { item, cardView, forecastIcon, dayOfWeek, temp, tempMaxMin ->
        }

        binding.recyclerForecast.adapter = adapter
        binding.recyclerForecast.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        postponeEnterTransition()
        binding.recyclerForecast.viewTreeObserver
            .addOnPreDrawListener {
                startPostponedEnterTransition()
                true
            }
    }

    private fun initForecast(list: List<ListItem>) {
        (binding.recyclerForecast.adapter as ForecastAdapter).submitList(list)
    }
}
