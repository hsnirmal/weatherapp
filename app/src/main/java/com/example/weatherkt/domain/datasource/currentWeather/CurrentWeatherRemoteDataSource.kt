package com.example.weatherkt.domain.datasource.currentWeather

import com.example.weatherkt.domain.WeatherAppAPI
import com.example.weatherkt.domain.model.CurrentWeatherResponse
import io.reactivex.Single
import javax.inject.Inject


class CurrentWeatherRemoteDataSource @Inject constructor(private val api: WeatherAppAPI) {

    fun getCurrentWeatherByGeoCords(lat: Double, lon: Double, units: String): Single<CurrentWeatherResponse> = api.getCurrentByGeoCords(lat, lon, units)
}
