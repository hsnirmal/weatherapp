package com.example.weatherkt.domain.datasource.currentWeather

import com.example.weatherkt.db.dao.CurrentWeatherDao
import com.example.weatherkt.db.entity.CurrentWeatherEntity
import com.example.weatherkt.domain.model.CurrentWeatherResponse
import javax.inject.Inject


class CurrentWeatherLocalDataSource @Inject constructor(private val currentWeatherDao: CurrentWeatherDao) {

    fun getCurrentWeather() = currentWeatherDao.getCurrentWeather()

    fun insertCurrentWeather(currentWeather: CurrentWeatherResponse) = currentWeatherDao.deleteAndInsert(
        CurrentWeatherEntity(currentWeather)
    )
}
